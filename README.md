# Flask Application

> Author: Ľuboslav Motošický 
>
> Subject: B0B37NSI
>
> Miniprojects: [Assignments of miniprojects](https://cw.fel.cvut.cz/wiki/courses/b0b37nsi/miniprojects/start)


# MiniProject 01

Design a Flask app for temperature monitoring using the Bootstrap library and the Jinja template engine. Create the project using the virtual environment and then upload it to the faculty Gitlab. Then upload a link to this repository to BRUTE, which will be set to either internal or public visibility.

## The application will have three pages: login form, registration form, dashboard.

### Templates for pages

- Base html document: [base.html](/app/templates/base.html)
- Index html document: [index.html](/app/templates/index.html)
- Login html document: [login.html](/app/templates/login.html)
- Registration html document: [register.html](/app/templates/register.html)
- Dashboard html document: [dashboard.html](/app/templates/dashboard.html)
- 404 html document: [404.html](/app/templates/404.html)


### The application will use template inheritance for navbar and page body.

Each web page uses the [base.html](/app/templates/base.html) file as a baseline, where the 'Main Section' section is used to render the part of the application that the user is currently using (e.g. dashboard.html).

Main Section from [base.html](/app/templates/base.html):
```html
    <!-- Main Section -->
    <div id="main-container" class="container">
        {% block content %}{% endblock %}
    </div>
```

Example usage in [dashboard.html](/app/templates/dashboard.html):
```html
{% extends "base.html" %}

{% block title %}Dashboard{% endblock %}

{% block content %}
<div class="container mt-5">
    <div class="header-section text-center mb-5">
        <h1>Temperature Dashboard</h1>
        <p class="lead">Monitor and manage temperature data effectively</p>
    </div>
...SNIP...
```

### Navigation Bar

The navbar will contain: the name of the page, the name of the logged in user (for now any text that will be passed to the templates as a variable), the logout button.

Webroute to [index.html](/app/templates/index.html) with predefined username variable:
```python
@web_routes_bp.route('/')
def index():
    username = "Guest"
    return render_template('index.html', user=username)
```

HTML template in [base.html](/app/templates/base.html) for navigation bar:
```html
    <!-- Navigation Bar Section -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark px-5">
        <a class="navbar-brand" href="/">Flask Application</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-between" id="navbarNav">
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="/login">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/register">Register</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/dashboard">Dashboard</a>
                </li>
            </ul>
            <div class="container-fluid">
                <span class="navbar-text text-success">
                    {{ user }}
                </span>
            </div>
            <form class="form-inline my-2 my-lg-0 d-flex">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
            </form>
        </div>
    </nav>
```

### Dashboard Layout

Full dashboard layout can be seen in Dashboard html document: [dashboard.html](/app/templates/dashboard.html)

![Dashboard Screenshot](/app/assets/dashboard_1.png)

### Measured Values Simulation

For this purpose I used a simple NoSQL database in json format available: [measurement-values.json](/app/model/measurement-values.json)


```json
[
    {
        "id": 2,
        "timestamp": "2024-02-28 13:00:00",
        "temp": 15.9
    },
    {
        "id": 3,
        "timestamp": "2024-02-28 14:00:00",
        "temp": 23.5
    },
    {
        "id": 4,
        "timestamp": "2024-02-28 15:00:00",
        "temp": 24.0
    },
...SNIP...
```

# MiniProject 02

Implement the REST API into an existing Flask application

Separate the URI endpoints for the API from the other endpoints in a custom api_routes.py file.
The endpoints will have the prefix /api/<api_endpoint>.

For the functionality of API endpoints on server side was implemented [apiroutes.py](/app/routes/apiroutes.py) file.

For the functionality of API endpoints and asynchronous calls it was necessary to implement javascript AJAX functionality which is available in: [dashboard_routes.js](app/static/dashboard_routes.js).

For the functionality realted to processing and handling json data on server side was implemented [data_handler.py](app/control/data_handler.py).

> Project provides functionality of: **Adding new item**, **Get n items**, **Updating an item via modal** and **Deleting an item**. 

# MiniProject 03
