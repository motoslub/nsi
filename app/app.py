# External Libraries
from flask import Flask, render_template, request, redirect, url_for

# API Routes
from routes import register_routes

app = Flask(__name__)
register_routes(app=app)

# Main Method
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True, use_reloader=True)