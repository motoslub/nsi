from flask import Blueprint

from .webroutes import web_routes_bp
from .apiroutes import api_routes_bp


def register_routes(app):
    app.register_blueprint(web_routes_bp)
    app.register_blueprint(api_routes_bp)