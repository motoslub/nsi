from flask import Blueprint, render_template

from control.data_handler import load_data, calculate_average
web_routes_bp = Blueprint('web_routes', __name__)


@web_routes_bp.route('/')
def index():
    username = "Guest"
    return render_template('index.html', user=username)

@web_routes_bp.route('/register')
def register():
    return render_template('register.html')

@web_routes_bp.route('/login')
def login():
    return render_template('login.html')

@web_routes_bp.route('/dashboard')
def dashboard():
    data = load_data()
    average_temp = round(calculate_average(data=data), 1)
    return render_template('dashboard.html', data=data, average_temp=average_temp)