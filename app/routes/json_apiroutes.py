"""
    This is backup file for API Routes with json format database of measurement values 
"""

from flask import Blueprint, request, jsonify

from app.control import data_handler

# Create a Blueprint for the API
api_routes_bp = Blueprint('api_routes', __name__)

# Example API endpoint
@api_routes_bp.route('/api/items', methods=['GET'])
def get_items():
    items_request = request.args.get('items', type=int)

    data = data_handler.load_data()
    if items_request: data = data[:items_request]

    average_temp = data_handler.calculate_average(data=data)
    
    response_data = {
        "data": data,
        "average_temp": average_temp
    }

    return jsonify(response_data)


@api_routes_bp.route('/api/items/<int:item_id>', methods=['PUT'])
def update_item(item_id):
    try:
        new_data = request.get_json()
        if data_handler.update_data(item_id, new_data):
            return jsonify({"success": True, "message": "Item updated successfully"}), 200
        else:
            return jsonify({"success": False, "message": "Item not found"}), 404
    except Exception as e: return jsonify({"success": False, "message": str(e)}), 500

@api_routes_bp.route('/api/items/<int:item_id>', methods=['DELETE'])
def delete_item(item_id):
    data = data_handler.load_data()
    success = data_handler.delete_record(item_id, data)

    if success: return jsonify({"success": True, "message": "Item deleted successfully"}), 200
    else: return jsonify({"success": False, "message": "Item not found"}), 404
    
@api_routes_bp.route('/api/items', methods=['POST'])
def add_item():
    try:
        new_data = request.get_json()
        if data_handler.create_record(new_data):
            return jsonify({"success": True, "message": "Item added successfully"}), 201
        else:
            return jsonify({"success": False, "message": "Failed to add item"}), 400
    except Exception as e:
        return jsonify({"success": False, "message": str(e)}), 500
