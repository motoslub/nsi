$(document).ready(function() {
    // Handle adding new item
    $('#dataInsertForm').on('submit',function(e) {
        e.preventDefault(); 
        let timestamp = $('#new_timestamp').val();
        let temp = $('#new_temp').val();

        $.ajax({
            url: '/api/items',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({ timestamp: timestamp, temp: temp }),
            success: function(response) {
                console.log("Item added", response);
                window.location.reload()
            },
            error: function(xhr) {
                console.error("Error adding new item", xhr.responseText);
            }
        });
    });

    // Handle deleting item
    $('.delete-item').on('submit',function(e) {
        e.preventDefault();
        let itemId = $(this).find('input[name="item-id"]').val();
        
        $.ajax({
            url: '/api/items/' + itemId,
            type: 'DELETE',
            success: function(response) {
                console.log("Item deleted", response);
                $('input[name="item-id"][value="' + itemId + '"]').closest('tr').remove();
            },
            error: function(xhr) {
                console.error("Error deleting item", xhr.responseText);
            }
        });
    });

    // Handle updating item

    // When any 'Update' button is clicked...
    $('.update-item-btn').click(function() {
        // Get current data from the button's data attributes
        let itemId = $(this).data('item-id');
        let itemTimestamp = $(this).data('item-timestamp');
        let itemTemp = $(this).data('item-temp');

        // Populate the form in the modal with current values
        $('#update_timestamp').val(itemTimestamp);
        $('#update_temp').val(itemTemp);
        $('#update_item_id').val(itemId);

        // Show the modal
        $('#updateItemModal').modal('show');
    });

    // When the form is submitted...
    $('#submitUpdate').click(function(e) {
        e.preventDefault();

        let itemId = $('#update_item_id').val();
        let updatedTimestamp = $('#update_timestamp').val();
        let updatedTemp = $('#update_temp').val();

        $.ajax({
            url: '/api/items/' + itemId,
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify({ timestamp: updatedTimestamp, temp: updatedTemp }),
            success: function(response) {
                $('#updateItemModal').modal('hide');
                // Refresh data on the page...
                window.location.reload()
            },
            error: function(xhr, status, error) {
                console.error("Error updating item:", error);
            }
        });
    });

    // Handle get items
    $('.dropdown-get-item').click(function(e) {
        e.preventDefault();
        let items = $(this).data('items');
        
        $.ajax({
            url: '/api/items',
            type: 'GET',
            data: { items: items },
            success: function(response) {
                // Assuming 'response' contains 'data' and 'average_temp'
                let data = response.data;
                let averageTemp = response.average_temp;
                let html = '';

                // Construct the table rows based on fetched data
                $.each(data, function(index, item) {
                    html += '<tr>' +
                            '<td>' + item.timestamp + '</td>' +
                            '<td>' + item.temp + ' °C</td>' +
                            '<td id="action-column">' +
                            '<button type="button" class="btn btn-danger btn-sm delete-item" data-item-id="' + item.id + '">Delete</button>' +
                            '<button type="button" class="btn btn-warning btn-sm update-item" data-item-id="' + item.id + '">Update</button>' +
                            '</td>' +
                            '</tr>';
                });

                 $('#dashboard-content .table-responsive .table tbody').html(html);
            },
            error: function(xhr, status, error) {
                console.error("Error fetching items:", error);
            }
        });
    });
});
