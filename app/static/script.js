
// Data Insert Validation
document.addEventListener('DOMContentLoaded', function() {
    const dataInsertForm = document.getElementById('dataInsertForm');
    dataInsertForm.addEventListener('submit', function(event) {
        let isValid = true;

        // Validate Timestamp
        let timestampInput = document.getElementById('new_timestamp');
        let timestampValue = timestampInput.value;
        let timestampPattern = /^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/; // YYYY-MM-DD HH:MM:SS
        if (!timestampPattern.test(timestampValue)) {
            alert('Timestamp must be in the format YYYY-MM-DD HH:MM:SS');
            isValid = false;
        }

        // Validate Temperature
        let tempInput = document.getElementById('new_temp');
        let tempValue = parseFloat(tempInput.value);
        if (isNaN(tempValue) || tempValue < -273.15) { // Check for valid number and above absolute zero
            alert('Please enter a valid temperature above -273.15 °C');
            isValid = false;
        }

        if (!isValid) event.preventDefault(); // Prevent form submission
    });
});