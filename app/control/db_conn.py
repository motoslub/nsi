import sqlite3
import json

DB = 'app/flaskDb.db'

def get_data():
    try:
        with sqlite3.connect(DB) as conn:
            conn.row_factory = sqlite3.Row  # Set row factory to access columns by name
            cursor = conn.cursor()
            get_data_query = "SELECT * FROM Measurement"
            cursor.execute(get_data_query)
            data_rows = cursor.fetchall()
            data_dicts = [dict(ix) for ix in data_rows]
            return json.dumps(data_dicts)
    except sqlite3.Error as e:
        print(f"Database error: {e}")
        return json.dumps({"error": "Database operation failed"})
    except Exception as e:
        print(f"An error occurred: {e}")
        return json.dumps({"error": "An unexpected error occurred"})

def insert_data(data):
    try:
        with sqlite3.connect(DB) as conn:    
            cursor = conn.cursor()
            insert_data_query = "INSERT INTO Measurement (timestamp, temp) VALUES (?, ?)"
            cursor.execute(insert_data_query, (data['timestamp'], data['temp']))
            conn.commit()
            conn.close()
    except sqlite3.Error as e:
        print(f"Database error: {e}")
    except Exception as e:
        print(f"An error occurred: {e}")
    
def delete_data(id):
    try:
        with sqlite3.connect(DB) as conn:
            cursor = conn.cursor()
            delete_data_query = "DELETE FROM Measurement WHERE id = ?"
            cursor.execute(delete_data_query, (id,))
            return True
    except sqlite3.Error as e:
        print(f"Database error: {e}")
    except Exception as e:
        print(f"An error occurred: {e}")
    return False
    
def update_data(id, data):
    try:
        with sqlite3.connect(DB) as conn:
            cursor = conn.cursor()
            update_data_query = "UPDATE Measurement SET timestamp = ?, temp = ? WHERE id = ? "
            cursor.execute(update_data_query, (data['timestamp'], data['temp'], id))
            return True
    except sqlite3.Error as e:
        print(f"Database error: {e}")
    except Exception as e:
        print(f"An error occurred: {e}")
    return False