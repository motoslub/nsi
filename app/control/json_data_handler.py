import json

# Load Complete JSON Data
def load_data():
    with open('app/model/measurement-values.json', 'r') as file:
        data = json.load(file)
    return data

# Load by Items Value
def load_data_by_items(items):
    data = load_data()
    return data[:items]

# Save New Record
def save_data(data):
    with open('app/model/measurement-values.json', 'w') as file:
        json.dump(data, file, indent=4)

def delete_record(item_id, data):
    # Find the item in the data
    item_to_delete = next((item for item in data if item['id'] == item_id), None)
    # Item with given ID not found
    if not item_to_delete: return False  
    
    data = [item for item in data if item['id'] != item_id]
    save_data(data)

    return True

def create_record(data):
    try:
        data['temp'] = int(data['temp'])
        with open('app/model/measurement-values.json', 'r+') as file:
            file_data = json.load(file)
            file_data.append(data) # Add the new data
            file.seek(0)
            json.dump(file_data, file, indent=4)
        return True
    except Exception as e:
        print(f"An error occurred: {e}")
        return False

def update_data(item_id, new_data):
    data = load_data()
    for item in data:
        if item['id'] == item_id:
            item['timestamp'] = new_data['timestamp']
            item['temp'] = float(new_data['temp'])
            save_data(data)
            return True
    return False

# Calculation of Average Temperature
def calculate_average(data):
    total_temp = sum(item['temp'] for item in data)
    average_temp = total_temp / len(data) if data else 0
    return average_temp


