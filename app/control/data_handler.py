from .db_conn import get_data, insert_data, delete_data, update_data
import json

# Load Complete JSON Data
def load_data():
    return json.loads(get_data())

# Load by Items Value
def load_data_by_items(items):
    data = load_data()
    return data[:items]

# Save New Record
def save_data(data):
    with open('app/model/measurement-values.json', 'w') as file:
        json.dump(data, file, indent=4)

def delete_record(item_id):
    return delete_data(item_id)

def create_record(data):
    try:
        data['temp'] = float(data['temp'])
        insert_data(data=data)
        return True
    except Exception as e:
        print(f"An error occurred: {e}")
        return False

def update_record(item_id, new_data):
    return update_data(item_id, new_data)

# Calculation of Average Temperature
def calculate_average(data):
    total_temp = sum(float(item['temp']) for item in data)
    average_temp = total_temp / len(data) if data else 0
    return average_temp
