
# Dashboard Data Management
@app.route('/dashboard')
def dashboard():
    DEFAULT = 5
    # GET Method with items parameter 
    items = request.args.get('items', default=DEFAULT, type=int)
    # Data Handler for Loading Data
    data = load_data()[:items] 
    # Processing of Average Temperature
    total_temp = sum(item['temp'] for item in data)
    average_temp = total_temp / len(data) if data else 0
    
    return render_template('dashboard.html', data=data, user=username, average_temp=average_temp)



# API Endpoints
@app.route('/get-items')
def get_items():
    # TODO
    return None

@app.route('/update-item', methods=['POST'])
def update_item():
    # TODO
    return None

@app.route('/delete-item', methods=['POST'])
def delete_item():
    item_id = request.form.get('id', type=int)
    data = load_data()
    data = [item for item in data if item['id'] != item_id]
    save_data(data)
    return redirect(url_for('dashboard'))

@app.route('/add-item', methods=['POST'])
def add_item():
    new_temp = request.form.get('new_temp', type=float)
    new_timestamp = request.form.get('new_timestamp')
    if new_temp and new_timestamp:
        data = load_data()
        # Check if there is data to avoid IndexError for empty list
        item_id = data[-1]['id'] + 1 if data else 1
        data.append({"id":item_id, "timestamp": new_timestamp, "temp": new_temp})
        save_data(data)
    return redirect('/dashboard')

# Error Management
@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404